%%% Simulacija na procesot od primerot 1 vo vezba 8 (Simulacioni rezultati)  
%%% so zadadeni vredosti na q0,q1,q2 za razlicni vrednosti na koeficientot r.
clc;
clear;
clf;
cla;
r = [0 0.1 0.25];   % vektorot na vrednosti na r
No_of_Values = 3;   % broj na razlicni vrednosti na r
T = 4;              % perioda na sempliranje
% Definiranje na vektorite na vrednosti na performansnite kriteriumi:
Se = zeros(1,No_of_Values);
Su = zeros(1,No_of_Values);
Seu = zeros(1,No_of_Values);
ymax = zeros(1,No_of_Values);
k = zeros(1,No_of_Values);
% Ovie vrednosti za parametrite na upravuvacot se zadavaat za sekoja site razlicni vrednosti na r:
q0 = [2.332 1.933 1.633]; 
q1 = [-3.076 -2.432 -2.016];
q2 = [1.117 0.816 0.637];
% Vrednostite na parametrite na procesot se isti bidejki T ne se menuva:
a1 = -1.0382;
a2 = 0.2466;
b1 = -0.07357;
b2 = 0.28197;

for i=1:No_of_Values       % Za sekoja mozna vrednost na r:
    
    w = 1.0; % edinecen otskocen vlez
    u_1 = 0; % minati vrednosti na upravuackata golemina
    u_2 = 0; % vo odnos na momentot koga se vrsi presmetkata
    y_1 = 0; % minati vrednosti na izlezot
    y_2 = 0; % vo odnos na momentot koga se vrsi presmetkata
    e_1 = 0; % minati vrednosti na greskata
    e_2 = 0; % vo odnos na momentot koga se vrsi presmetkata
    Max_Time = 128; % vreme na simulacija
    No_of_Samples = Max_Time/T;  % broj na primeroci
    flag = 0; % flag koj pokazuva dali e zacuvano vremeto na porast
    
    Tvec = zeros(1,No_of_Samples);  % vektor vo koj se cuvaat vrednostite na momentite na sempliranje
    uvec = zeros(1,No_of_Samples);  % vektor vo koj se cuva istorijata na vrednosti na upravuvackiot signal
    yvec = zeros(1,No_of_Samples);  % vektor vo koj se cuva istorijata na vrednosti na izlezniot signal
    evec = zeros(1,No_of_Samples);  % vektor vo koj se cuva istorijata na vrednosti na greskata
    
    for n = 1:No_of_Samples         % Za sekoj primerok:
        
        y = -a1*y_1 - a2*y_2 + b1*u_1 + b2*u_2;       % diferentna ravenka na objektot na upravuvanje
        e = w-y;                                      % vrednost na greskata
        if abs(e) < 0.05*w && flag == 0               % Ako greskata padne pod 5%:
            k(i) = n;                                 % zacuvaj go vremeto na porast
            flag = 1;                                 % i ne proveruvaj poveke
        end
        u = u_1 + q0(i)*e + q1(i)*e_1 + q2(i)*e_2;    % diferentna ravenka na upravuvacot
        % Update na vektorite na istorija:
        e_2 = e_1;
        e_1 = e;
        u_2 = u_1;
        u_1 = u;
        y_2 = y_1;
        y_1 = y;
        Tvec(n) = (n-1)*T; 
        uvec(n) = u; 
        yvec(n) = y; 
        evec(n) = e; 
        
    end
    
    figure(i)
    subplot(2,1,1),plot(Tvec,uvec,'b'); grid; % se iscrtuva upravuvackiot signal 
    subplot(2,1,2),plot(Tvec,yvec,'r'); grid; % se iscrtuva izlezot 

    Esum2 = 0;                                % suma od kvadratite na greskite
    for j=1:No_of_Samples
        Esum2 = Esum2 + (evec(j))^2;
    end
    Se(i) = ((1/(No_of_Samples+1))*Esum2)^(1/2); % koren od srednoto kvadratno otstapuvanje (kriterium Se)
    
    uBeskraj = uvec(No_of_Samples);         % stacionatna vrednost na upravuvackiot signal
    Delta_u_sum2 = 0;                       % suma od kvadratite na upravuvackoto otstapuvanje
    for j=1:No_of_Samples
        Delta_u_sum2 = Delta_u_sum2 + (uvec(j)-uBeskraj)^2;
    end
    Su(i) = ((1/(No_of_Samples+1))*Delta_u_sum2)^(1/2);   % koren od srednoto kvadratno upravuvacko otstapuvanje (kriterium Su)
    
    Seu(i) = Se(i) + Su(i);     % kriterium Seu
    ymax(i) = max(yvec);      % maksimalen preskok 
    
end 

figure(No_of_Values+1)               % Se crta zavisnosta na performansnite kriteriumi od koeficientot r
grid
hold on
plot(r, Su, 'r')
plot(r, Se, 'b')
plot(r, Seu, 'k')
plot(r, ymax, 'm')
plot(r, k.*0.1, 'g')    % mnozime so 0.1 za da e relativno so drugite vrednosti na istiot grafik 
hold off 